#include <stdio.h>
#include <getopt.h>
#include <unistd.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <ctype.h>

#define OPTSTR "f:au:n:"
#define USAGE_STR "Usage: %s [-f filename <-a> <-u pid> <-n process name>] | <-a> <-u pid> <-n process name>\n" \
                  "Only one argument in angle brackets is allowed, and one of them is required.\n" \
                  "Optional argument -f requires one of the mandatory arguments\n\n"


void usage_prompt(const char* prog, const char* error);
char* status_parser_by_pid(const char* pid);
char* status_parser_by_name(const char* pid, const char* process_name);
void print_results(const char* printstring, const int optional_flag, const char* filename);

int main(int argc, char* argv[])
{
    extern int optind;
    int opt, mandatory_flag = 0, optional_flag = 0;
    char* pid;
    char* filename = NULL, *processname, *printstring = NULL;

    DIR* proc = opendir("/proc");
    struct dirent* ent;

    if (proc == NULL) {
        exit(EXIT_FAILURE);
    }

    if (argc < 2)
        usage_prompt(argv[0], "At least one argument is required.\n");

    while ((opt = getopt(argc, argv, OPTSTR)) != EOF)
    {
        switch (opt)
        {
            case 'f':
                if (optind >= argc)
                    usage_prompt(argv[0], "Invalid order of arguments.\n");

                optional_flag = 1;
                filename = optarg;
                break;

            case 'a':
                if (mandatory_flag)
                    usage_prompt(argv[0], "Only one argument from angle brackets is allowed.\n");

                if (argv[optind])
                    usage_prompt(argv[0], "This argument takes no value.\n");
                
                mandatory_flag = opt;
                break;

            case 'u':
                if (mandatory_flag)
                    usage_prompt(argv[0], "Only one argument from angle brackets is allowed.\n");

                if (strspn(optarg, "0123456789") != strlen(optarg))
                    usage_prompt(argv[0], "Invalid argument value\n");
                    
                mandatory_flag = opt;
                pid = optarg;     
                break;

            case 'n':
                if (mandatory_flag)
                    usage_prompt(argv[0], "Only one argument from angle brackets is allowed.\n");

                mandatory_flag = opt;
                processname = optarg;
                break;

            default:
                usage_prompt(argv[0], "Unknown argument.\n");
        }
    }

    switch (mandatory_flag)
    {
        case 97:
            while (ent = readdir(proc)) 
            {
                if (!isdigit(*ent->d_name))
                    continue;

                printstring = status_parser_by_pid(ent->d_name);
                print_results(printstring, optional_flag, filename);
                free(printstring);
            }
            break;

        case 117:
        {
            int found_pid = 0;
            while (ent = readdir(proc))
            {
                if (strcmp(ent->d_name, pid))
                    continue;

                printstring = status_parser_by_pid(pid);
                print_results(printstring, optional_flag, filename);
                free(printstring);
                found_pid = 1;
                break;
            }
            if (!found_pid)
                fprintf(stderr, "Unknown PID\n");
            break;
        }
            
        case 110: 
        {
            int found_process = 0;
            while (ent = readdir(proc))
            {
                if (!isdigit(*ent->d_name))
                    continue;

                printstring = status_parser_by_name(ent->d_name, processname);
                
                if (!printstring)
                    continue;

                print_results(printstring, optional_flag, filename);
                free(printstring);
                found_process = 1;
            }
            if (!found_process)
                fprintf(stderr, "Unknown process name.\n");
            break;
        }
            
        default:
            usage_prompt(argv[0], "Unknown argument.\n");
    }

    closedir(proc);
    return EXIT_SUCCESS;
}

void usage_prompt(const char* prog, const char* error)
{
    fprintf(stderr, "%s", error);
    fprintf(stderr, USAGE_STR, prog);
    exit(EXIT_FAILURE);
}

char* status_parser_by_pid(const char* pid) {
    char path[40], line[100];
    FILE* statusf;

    char name_buffor[17];

    snprintf(path, 40, "/proc/%s/status", pid);

    statusf = fopen(path, "r");
    if (!statusf)
    {
        fprintf(stderr, "%s", "Cannot open status file.\n");
        exit(EXIT_FAILURE);
    }
        

    while (fgets(line, 100, statusf)) {
        if (strncmp(line, "Name:", 5) == 0)
        {
            strcpy(name_buffor, line + 6);
            break;
        }
    }
    int memsize = strlen(name_buffor) + strlen(pid) + 2;
    char* printstring = (char*)malloc(memsize);

    snprintf(printstring, memsize, "%s %s", pid, name_buffor);
    //printf("%s", printstring);
    fclose(statusf);
    return printstring;
    //free(printstring);
}

char* status_parser_by_name(const char* pid, const char* process_name) {
    char path[40], line[100];
    FILE* statusf;

    char name_buffor[17];

    snprintf(path, 40, "/proc/%s/status", pid);

    statusf = fopen(path, "r");
    if (!statusf)
    {
        fprintf(stderr, "%s", "Cannot open status file.\n");
        exit(EXIT_FAILURE);
    }

    while (fgets(line, 100, statusf)) {
        if (!strncmp(line, "Name:", 5))
        {
            strcpy(name_buffor, line + 6);
            break;
        }
    }
    int x = strlen(process_name);
    if (strncmp(name_buffor, process_name, strlen(process_name)))
        return NULL;

    int memsize = strlen(name_buffor) + strlen(pid) + 2;
    char* printstring = (char*)malloc(memsize);

    snprintf(printstring, memsize, "%s %s", pid, name_buffor);
    //printf("%s", printstring);
    fclose(statusf);
    return printstring;
}

void print_results(const char* printstring, const int optional_flag, const char* filename)
{
    //zapis do pliku
    if (optional_flag)
    {
        FILE* resultf;
        resultf = fopen(filename, "a");
        if (!resultf)
        {
            fprintf(stderr, "%s", "Cannot open or create file.\n");
            exit(EXIT_FAILURE);
        }

        fwrite(printstring, 1, strlen(printstring), resultf);
        fclose(resultf);   
    }
    //zapis do konsoli
    else
        printf("%s", printstring);
}